<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public $timestamps = false;
    protected $fillable = ['id', 'date', 'description', 'user_id'];

    public function groups()
    {
        return $this->belongsToMany('App\Group');
    }

    public function user()
    {
        //UN EVENTO ES CREADO POR UN UNICO USUARIO
        return $this->belongsTo('App\User');
    }

    public function users()
    {
        //MUCHOS EVENTOS PUEDEN SER DIRIGIDOS POR MUCHOS USUARIOS
        return $this->belongsToMany('App\User');
    }
}
