<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public $timestamps = false;
    protected $fillable = ['id', 'group'];

    public function events()
    {
        return $this->belongsToMany('App\Event');
    }
}
