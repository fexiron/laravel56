<?php

namespace App\Http\Controllers\API;

use App\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    public function index()
    {
        $event = Event::with('users','user')->get();
        return $event;
    }

    public function destroy($id)
    {
        $event = Event::find($id);
        if(!$event){
            return response()->json([
                'message' => 'Event not find'
            ], 404);
        }
        $event->delete();
        return ['deleted' => $id];
    }

    public function show($id)
    {
        $event = Event::find($id);

        if($event){
            return $event;
        }else{
            return response()->json([
                'message' => 'Record not found',
            ], 404);
        }
    }
}

