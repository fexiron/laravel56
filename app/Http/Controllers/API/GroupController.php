<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Group;

class GroupController extends Controller
{
       public function index()
    {
        $group = Group::with('events')->get();
        return $group;
    }
}
