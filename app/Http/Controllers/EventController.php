<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use Auth;
use App\User;
use App\Group;
use PDF;
use Mail;
use App\Mail\EventShipped;

class EventController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', Event::class);
        $events = Event::with('user.role')->paginate();
        //return $events;
        return view('event.index', ['events' => $events]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('event.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validacion trait
        $this->validate($request, [
            'date' => 'required|max:10|date',
            'description' => 'required|max:255',
        ]);
        $this->authorize('create', \App\Event::class);
        $event = new Event($request->all());
        $event->user_id = Auth::user()->id;
        $event->save();
        $request->session()->put('lastEvent', $event->id);
        return redirect('/events');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::where('id', $id)->first();
        $this->authorize('view', $event);
        //$this->authorize('view', $event);
        return view('event.show', ['event' => $event]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::where('id', $id)->first();
        $this->authorize('update', $event);
        $groups = Group::all();
        $users = User::all();
        return view('event.edit', [
            'event' => $event,
            'groups' => $groups,
            'users' => $users
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'date' => 'required|max:10|date',
            'description' => 'required|max:255',
        ]);
        $event = Event::find($id);
        $this->authorize('update', $event);
        $event->fill($request->all());
        $event->save();
        return redirect('/events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Event::destroy($id);
        return redirect("/events");
    }

    public function addGroup(Request $request, $id)
    {
        //buscar el evento.
        $event = Event::find($id);
        $groupId = $request->input('sel1');
        //agregar o sincronizar el elemento.
        // $event->groups()->attach($groupId);
        $event->groups()->syncWithoutDetaching($groupId);
        //reenvio a la misma ruta
        return redirect("/events/$id/edit");
    }

    public function deleteGroup(Request $request, $idEvent)
    {
        $groups = $request->input('group');
        $idGroup = $groups[0];
        $event = Event::find($idEvent);
        $event->groups()->detach($idGroup);
        return redirect("/events/$idEvent/edit");
    }

    public function addUser(Request $request, $id)
    {
        //buscar el evento.
        $event = Event::find($id);
        $userId = $request->input('sel2');
        //agregar o sincronizar el elemento.
        // $event->groups()->attach($userId);
        $event->users()->syncWithoutDetaching($userId);
        //reenvio a la misma ruta
        return redirect("/events/$id/edit");
    }

    public function deleteUser(Request $request, $idEvent)
    {
        $users = $request->input('user');
        $idUser = $users[0];
        $event = Event::find($idEvent);
        $event->users()->detach($idUser);
        return redirect("/events/$idEvent/edit");
    }

    public function mail(Request $request, $idEvent)
    {
        $event = Event::find($idEvent);
        $creator = User::find($event->user_id);

        $data = array(
            'event' => $event,
            'creator' => $creator
        );

        $pdf = PDF::loadView('letters.test', $data);


        foreach ($event->users as $user){
            Mail::send('emails.orders.send', $data, function($message) use($pdf, $user)
            {
                $message->from('events@mailing.com', 'Automail Event');

                $message->to($user->email)->subject('Tienes un nuevo evento!');

                $message->attachData($pdf->output(), "nuevo_evento.pdf");
            });
        }

        // Mail::send('emails.invoice', $data, function($message) use($pdf)
        // {
        //     $message->from('events@mailing.com', 'Automail Event');

        //     $message->to($creator->email)->subject('Has credao un nuevo evento!');

        //     $message->attachData($pdf->output(), "invoice.pdf");
        // });

        // Mail::to($creator->email)->send(new EventShipped($event, $creator));

        return redirect("/events");
    }
}
