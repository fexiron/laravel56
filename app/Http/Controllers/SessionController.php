<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function text()
    {
        return view('session.text');
    }

    public function storeText(Request $request)
    {
        $request->session()->put('persona.nombre', $request->input('nombre'));
        $request->session()->put('persona.apellido', $request->input('apellido'));

        // $request->session()->push('gente.nombre', $request->input('nombre'));
        // $request->session()->push('gente.apellido', $request->input('apellido'));


        // $article = \App\Article::all()->random(1)[0];

        // // dd($article);
        // $request->session()->push('articles', $article); //modo 2

        return back();
        return redirect('/home');
    }

    public function flush(Request $request)
    {
        $request->session()->flush();
        return back();
    }

    public function forget(Request $request)
    {
        $request->session()->forget('text');
        // $request->session()->forget('textos');
        $request->session()->forget('arraytext');

        // $request->session()->pull('arraytext[0]');

        // $request->session()->forget('varios');
        // $request->session()->forget('articles');
        return back();
        return redirect('/home');
    }


    public function simple()
    {
        return view('session.simple');
    }

    public function simplePost(Request $request)
    {
        $text = $request->input('text');
        //lo guardamos en la variable text:
        session(['text' => $text]); //modo 1
        $request->session()->put('text', $text); //modo 2

        $request->session()->push('arraytext', $text); //modo 2

        return back();
    }

    public function simpleForget(Request $request)
    {
        $request->session()->forget('text');
        return back();
    }

    public function simpleForgetArray(Request $request, $key)
    {
        $x = $request->session()->pull("arraytext.$key");
        return back();
    }

    public function forgetPersona(Request $request, $key)
    {
        return 1;
        $request->session()->pull("persona.$key");
        return back();
    }
}
