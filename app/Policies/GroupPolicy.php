<?php

namespace App\Policies;

use App\User;
use App\Event;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupPolicy
{
    use HandlesAuthorization;


    public function before($user)
    {
        if($user->role_id == 1){
            return true;
        }
    }

    /**
     * Determine whether the user can view the event.
     *
     * @param  \App\User  $user
     * @param  \App\Event  $event
     * @return mixed
     */
    public function view(User $user)
    {
        return true; //TODOS PUEDEN VERLO
        //return $user-> == $article->user_id; //SOLO EL PROPIETARIO
        //return $user->role_id == 2 || $user->id == $event->user_id;
    }

    /**
     * Determine whether the user can create events.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role_id == 1;
    }

    /**
     * Determine whether the user can update the event.
     *
     * @param  \App\User  $user
     * @param  \App\Event  $event
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->role_id == 1;
    }

    /**
     * Determine whether the user can delete the event.
     *
     * @param  \App\User  $user
     * @param  \App\Event  $event
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->role_id == 1;
    }

    public function index(User $user)
    {
        return true;
    }
}
