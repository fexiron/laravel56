<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')
        ->insert([
            'id' => 1,
            'date' => Carbon::createFromDate(2012, 2, 19, 'Europe/Madrid'),
            'description' => 'Descripción breve del evento'
        ]);
         DB::table('events')
        ->insert([
            'id' => 2,
            'date' => Carbon::createFromDate(2012, 2, 22, 'Europe/Madrid'),
            'description' => 'Descripción breve del evento'
        ]);
    }
}
