<?php

use Illuminate\Database\Seeder;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')
        ->insert([
            'id' => 1,
            'group' => 'daw2'
        ]);
        DB::table('groups')
        ->insert([
            'id' => 2,
            'group' => 'dad2'
        ]);
        DB::table('groups')
        ->insert([
            'id' => 3,
            'group' => 'dam2'
        ]);
    }
}
