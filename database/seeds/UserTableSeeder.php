<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
        ->insert([
            'id' => 1,
            'name' => 'alejandro',
            'email' => 'alejandro@gmail.com',
            'password' => bcrypt('pass'),
            'role_id' => 1
        ]);
        DB::table('users')
        ->insert([
            'id' => 2,
            'name' => 'patricio',
            'email' => 'patricio@gmail.com',
            'password' => bcrypt('pass'),
            'role_id' => 2
        ]);
        DB::table('users')
        ->insert([
            'id' => 3,
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret'),
            'role_id' => 1
        ]);
        DB::table('users')
        ->insert([
            'id' => 4,
            'name' => 'user',
            'email' => 'user@gmail.com',
            'password' => bcrypt('secret'),
            'role_id' => 2
        ]);
    }
}
