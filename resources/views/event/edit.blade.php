@extends('layouts.app')
@section('content')
<h1>Edición de evento</h1>

<form method="post" action="/events/{{ $event->id }}">
    {{ csrf_field() }}

    <input type="hidden" name="_method" value="put">
    <div class="form-group">
        <label>Id</label>
        <input type="text" name="id" class="form-control" value="{{ $event->id }}" disabled="true">
    </div>

    <div class="form-group">
        <label>Fecha</label>
        <input type="text" name="date" class="form-control" value="{{ $event->date }}">
    </div>

    <div class="form-group">
        <label>Descripcion</label>
        <input type="text" name="description" class="form-control" value="{{ $event->description }}">
    </div>

    <div class="alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </div>
    <div class="form-group">
    <input type="submit" name="" value="Guardar" class="form-control">
    </div>
</form>
<hr>
<div class="container">
    <div class="row">
        <div class="col-6">
            <form method="post" action="/events/{{ $event->id }}/groups">
                {{ csrf_field() }}
                <label for="sel1">Añadir grupo:</label>
                  <select class="form-control" name="sel1">
                    @foreach ($groups as $group)
                        <option value="{{ $group->id }}">{{ $group->group }}</option>
                    @endforeach
                  </select>
                <input type="submit" name="" value="Agregar grupo" class="form-control">
            </form>
        </div>
        <div class="col-6">
            <form  method="post" action="/events/{{ $event->id }}/groups">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <h3>Grupos asistentes:</h3>
                    @foreach ($event->groups as $group)
                        <div class="form-check form-check-inline">
                          <label class="form-check-label">
                          </label>
                        <input class="form-check-input" type="radio" name="group" id="inlineRadio{{ $group->id }}" value="{{ $group->id }}"> {{ $group->group }}
                        </div>
                    @endforeach
                <input type="submit" name="" value="Eliminar grupo" class="form-control">
            </form>
        </div>
    </div>
</div>
<hr>
<div class="container">
    <div class="row">
        <div class="col-6">
            <form method="post" action="/events/{{ $event->id }}/users">
                {{ csrf_field() }}
                <label for="sel1">Añadir usuario:</label>
                  <select class="form-control" name="sel2">
                    @foreach ($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                  </select>
                <input type="submit" name="" value="Agregar usuario" class="form-control">
            </form>
        </div>
        <div class="col-6">
            <form  method="post" action="/events/{{ $event->id }}/users">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <h3>Usuarios asistentes:</h3>
                    @foreach ($event->users as $user)
                        <div class="form-check form-check-inline">
                          <label class="form-check-label">
                          </label>
                        <input class="form-check-input" type="radio" name="user" id="inlineRadio{{ $user->id }}" value="{{ $user->id }}"> {{ $user->name }}
                        </div>
                    @endforeach
                <input type="submit" name="" value="Eliminar grupo" class="form-control">
            </form>
        </div>
    </div>
</div>
@endsection
