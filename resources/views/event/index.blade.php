@extends('layouts.app')
@section('content')
<h1>Lista de eventos</h1>
<table class="table table-bordered">
    <tr>
        <th>id</th>
        <th>fecha</th>
        <th>descripcion</th>
        <th>creador</th>
        <th>acciones</th>
    </tr>

    @foreach ($events as $event)
    @if($event->id == request()->session()->get('lastEvent'))
    <tr class="table-info">
    @else
    <tr>
    @endif

        <td>{{ $event->id }}</td>
        <td>{{ $event->date }}</td>
        <td>{{ $event->description }}</td>
        <td>{{ $event->user->name }}</td>
        <td>
            <form method="post" action="/events/{{ $event->id }}">
                {{ csrf_field() }}

                @can ('view', $event)
                <input type="hidden" name="_method" value="delete">
                <a class="btn btn-secondary" href="/events/{{ $event->id }}">Ver</a>
                @endcan

                @can ('update', $event)
                <a class="btn btn-secondary" href="/events/{{ $event->id }}/edit">Editar</a>
                @endcan

                @can ('delete', $event)
                <input class="btn btn-secondary" type="submit" name="Borrar" value="Borrar">
                @endcan
            </form>
        </td>
    </tr>
    @endforeach
</table>
<hr>
<a class="btn btn-secondary" href="/events/create">Nuevo</a>
@endsection
