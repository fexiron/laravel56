@extends('layouts.app')
@section('content')
<h1>Detalle de evento</h1>

<ul>
    <li>ID: <b>{{ $event->id }}</b></li>
    <li>FECHA: <b>{{ $event->date }}</b></li>
    <li>DESCRIPCION: <b>{{ $event->description }}</b></li>

    @can ('update', $event)
    <a class="btn btn-secondary" href="/events/{{ $event->id }}/mail">Avisar</a>
    @endcan

    <br><hr>

    <h3><b>Grupos asistentes:</b></h3>
    @foreach ($event->groups as $group)
        <li name="{{ $group->id }}">
            {{ $group->group }}
        </li>
    @endforeach

    <h3><b>Usuarios asistentes:</b></h3>
    @foreach ($event->users as $user)
        <li name="{{ $user->id }}">
            {{ $user->name }}
        </li>
    @endforeach
</ul>
@endsection
