@extends('layouts.app')
@section('content')
<h1>Alta de grupo</h1>

<form method="post" action="/groups">
    {{ csrf_field() }}

    <div class="form-group">
    <label>grupo</label>
    <input type="text" name="group" class="form-control" value="{{ old('group') }}">
    </div>

    <div class="alert-danger">
        {{$errors->first('group')}}
    </div>

     <div class="form-group">
    <input type="submit" name="" value="Nuevo" class="form-control">
    </div>
</form>

@endsection
