@extends('layouts.app')
@section('content')
<h1>Lista de grupos</h1>
<table class="table table-bordered">
    <tr>
        <th>id</th>
        <th>grupo</th>
        <th>acciones</th>
    </tr>

    @foreach ($groups as $group)
    <tr>
        <td>{{ $group->id }}</td>
        <td>{{ $group->group }}</td>
        <td>
            <form method="post" action="/groups/{{ $group->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">

                @can ('view', $group)
                <a class="btn btn-secondary" href="/groups/{{ $group->id }}">Ver</a>
                @endcan

                @can ('update', $group)
                <a class="btn btn-secondary" href="/groups/{{ $group->id }}/edit">Editar</a>
                @endcan

                @can ('update', $group)
                <input class="btn btn-secondary" type="submit" name="Borrar" value="Borrar">
                @endcan
            </form>
        </td>
    </tr>
    @endforeach
</table>
<hr>

@can ('create', $group)
<a class="btn btn-secondary" href="/groups/create">Nuevo</a>
@endcan
@endsection
