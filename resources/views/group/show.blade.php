@extends('layouts.app')
@section('content')
<h1>Detalle de grupo</h1>

<ul>
    <li>ID: <b>{{ $group->id }}</b></li>
    <li>GRUPO: <b>{{ $group->group }}</b></li>
    <br>
    <h3>Eventos pendientes:</h3>
    @foreach ($group->events as $event)
        <li name="{{ $event->id }}">
            {{ $event->description }}
        </li>
    @endforeach
</ul>


@endsection
