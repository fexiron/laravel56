@extends('layouts.app')
@section('content')
<h1>Lista de roles</h1>
<table class="table table-bordered">
    <tr>
        <th>id</th>
        <th>nombre</th>
    </tr>

    @foreach ($roles as $rol)
    <tr>
        <td>{{ $rol->id }}</td>
        <td>{{ $rol->name }}</td>
    </tr>
    @endforeach

</table>
@endsection
