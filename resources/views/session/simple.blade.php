@extends('layouts.app')

@section('content')

<div class="container">

<h1>Guardar texto en sesión</h1>

<div>
    <form method="post" action="/sesion/simple">
        {{ csrf_field() }}

        <div  class="form-group">
            <label>Texto</label>
            <input class="form-control"  type="text" name="text" value="">
        </div>

        <div class="form-group">
            <label></label>
            <input class="form-control"  type="submit" name="" value="Gurdar en sesión">
        </div>



    </form>
</div>

<div>
    <h2>Valor del texto guardado:</h2>
    <p>Leído de distintos modos:</p>

        Último texto guardado en sesión (con Session):

        <strong>
        {{ Session::get('text') }}
        </strong>

        <hr>
        Último texto guardado en sesión (con request()):
        <strong>
        {{ request()->session()->get('text') }}
        </strong>


        <hr>
        Último texto guardado en sesión (con session()):
        {{ session('text') }}

</div>

<div>
        <h2>Array de textos:</h2>
        <ul>
        @if (request()->session()->has('arraytext'))
        @foreach (session::get('arraytext') as $key => $item)
            <li>{{ $item }}:   <a href="/sesion/simpleForgetArray/{{ $key }}">Olvidar</a></li>
        @endforeach
        @endif
        </ul>
</div>


<div>
    <div class="alert alert-default">
        <a class="btn btn-warning" href="/sesion/simpleForget">Borrar datos selectivamente</a>
        <br>
        <br>
        <a class="btn btn-danger" href="/sesion/flush">Borrar datos de sessión: ojo que cerraremos al usuario logueado</a>
    </div>

</div>



    <hr>
    <pre>
        <?php

            var_dump(request()->session()->all());
         ?>
    </pre>
</div>
@endsection
