@extends('layouts.app')

@section('content')

<div class="container">

<h1>Guardar texto en sesión</h1>

<form method="post" action="/sesion/texto">
    {{ csrf_field() }}

    <div  class="form-group">
        <label>Nombre</label>
        <input class="form-control"  type="text" name="nombre" value="">
    </div>

    <div  class="form-group">
        <label>Apellido</label>
        <input class="form-control"  type="text" name="apellido" value="">
    </div>


    <div class="form-group">
        <label></label>
        <input class="form-control"  type="submit" name="" value="Nuevo">
    </div>



</form>

<div>
    <h2>Última persona:</h2>
    Nombre: {{ session()->get('persona.nombre') }}<br>
    Apellido: {{ session()->get('persona.apellido') }} <br>

    <hr>

    @foreach ( session()->get('persona') as $key => $item)
        <li>
            {{ $key }}: {{ $item }}
            <a href="/sesion/forgetPersona/{{ $key }}">Olvidar</a>
        </li>
    @endforeach

    <a href="/sesion/forgetPersona">Olvidar persona</a>


</div>

    <hr>
    <pre>
        <?php

            var_dump(request()->session()->all());
         ?>
    </pre>
    <div class="alert alert-default">
        <a class="btn btn-warning" href="/sesion/forget">Borrar datos selectivamente</a>
        <br>
        <br>
        <a class="btn btn-danger" href="/sesion/flush">Borrar datos de sessión: ojo que cerraremos al usuario logueado</a>
    </div>
</div>
@endsection
