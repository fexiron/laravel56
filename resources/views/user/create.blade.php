@extends('layouts.app')
@section('content')
<h1>Alta de usuraio</h1>

<form method="post" action="/users">
    {{ csrf_field() }}

    <div class="form-group">
    <label>date</label>
    <input type="text" name="date" class="form-control" placeholder="yyyy-mm-dd" value="{{ old('date') }}">
    </div>

    <div class="form-group">
    <label>description</label>
    <input type="text" name="description" class="form-control" value="{{ old('description') }}">
    </div>

    <div class="alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </div>

     <div class="form-group">
    <input type="submit" name="" value="Nuevo" class="form-control">
    </div>
</form>

@endsection
