@extends('layouts.app')
@section('content')
<h1>Edición de usuario</h1>

<form method="post" action="/users/{{ $user->id }}">
    {{ csrf_field() }}

    <input type="hidden" name="_method" value="put">

    <div class="form-group">
        <label>Nombre</label>
        <input type="text" name="name" class="form-control" value="{{ $user->name }}">
    </div>
    <div class="alert-danger">
        {{$errors->first('name')}}
    </div>

    <div class="form-group">
        <label>Email</label>
        <input type="text" name="email" class="form-control" value="{{ $user->email }}">
    </div>
    <div class="alert-danger">
        {{$errors->first('email')}}
    </div>

    <div class="form-group">
        <label>Rol</label>
        <br>
        <select name="role_id">
            @foreach ($roles as $role)
            <option value="{{ $role->id }}" {{ $role->id == $user->role_id ? 'selected'  : ''}}> {{ $role->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="alert-danger">
        {{$errors->first('role')}}
    </div>

    <div class="form-group">
        <input type="submit" name="" value="Editar" class="form-control">
    </div>
</form>

@endsection
