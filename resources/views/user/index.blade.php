@extends('layouts.app')
@section('content')
<h1>Lista de usuarios</h1>
<table class="table table-bordered">
    <tr>
        <th>id</th>
        <th>nombre</th>
        <th>email</th>
        <th>role</th>
        <th>acciones</th>
    </tr>

    @foreach ($users as $user)
    <tr>
        <td>{{ $user->id }}</td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->role->name }}</td>
        <td>
            @can ('view', $user)
            <a class="btn btn-secondary" href="/users/{{ $user->id }}">Ver</a>
            @endcan

            @can ('update', $user)
            <a class="btn btn-secondary" href="/users/{{ $user->id }}/edit">Editar</a>
            @endcan
        </td>
    </tr>
    @endforeach

</table>

{{ $users->links() }}
@endsection
