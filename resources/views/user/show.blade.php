@extends('layouts.app')
@section('content')
<h1>Detalle de usuario</h1>

<ul>
    <li>ID: <b>{{ $user->id }}</b></li>
    <li>NOMBRE: <b>{{ $user->name }}</b></li>
    <br>
    <h3>Eventos creados:</h3>
    @foreach ($user->events as $event)
        <li name="{{ $event->id }}">
            {{ $event->description }}
        </li>
    @endforeach
    <br>
    <h3>Eventos pendientes:</h3>
    @foreach ($user->eventsOn as $event)
        <li name="{{ $event->id }}">
            {{ $event->description }}
        </li>
    @endforeach
</ul>


@endsection
