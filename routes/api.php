<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/users', 'API\UserController@index');
Route::get('/roles', 'API\RoleController@index');
Route::get('/groups', 'API\GroupController@index');


Route::resource('/events', 'API\EventController', ['except' => ['create', 'edit']]);
Route::post('register', 'Api\AuthController@register');
Route::post('login', 'Api\AuthController@login');

Route::group(['middleware' => 'auth:api'], function ()
{
    Route::post('profile', 'Api\AuthController@profile');
});
