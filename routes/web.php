<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('groups', 'GroupController');
Route::resource('events', 'EventController');

Route::get('/roles', 'RoleController@index');
Route::resource('users', 'UserController');
Route::get('/home', 'HomeController@index')->name('home');

Route::post('/events/{id}/groups', 'EventController@addGroup');
Route::delete('/events/{id}/groups', 'EventController@deleteGroup');

Route::post('/events/{id}/users', 'EventController@addUser');
Route::delete('/events/{id}/users', 'EventController@deleteUser');

Route::get('/sesion/texto', 'SessionController@text');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/redirect/google', 'SocialAuthController@redirect');
Route::get('/callback/google', 'SocialAuthController@callback');


Route::get('/events/{id}/mail', 'EventController@mail');
