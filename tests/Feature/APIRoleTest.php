<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class APIRoleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRoleJSON()
    {
        $response = $this->json('GET', '/api/roles');

        $response
            ->assertStatus(200)
            ->assertExactJson([
                [
                    'id' => 1,
                    'name' => 'administrador',
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id' => 2,
                    'name' => 'usuario',
                    'created_at' => null,
                    'updated_at' => null
                ]
            ]);
    }
}
