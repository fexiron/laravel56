<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class APIUserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->json('GET', '/api/users');

        $response
            ->assertStatus(200)
            ->assertExactJson([
                [
                    'id' => 1,
                    'name' => 'alejandro',
                    'email' => 'alejandro@gmail.com',
                    'role_id' => 1,
                    'created_at' => null,
                    'updated_at' => null
                ],
                [
                    'id' => 2,
                    'name' => 'patricio',
                    'email' => 'patricio@gmail.com',
                    'role_id' => 2,
                    'created_at' => null,
                    'updated_at' => null
                ]
            ]);
    }
}
