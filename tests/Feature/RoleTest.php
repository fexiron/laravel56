<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RoleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->get('/roles');

        $response->assertStatus(200);
        $response->assertSee('id');
        $response->assertSee('nombre');
        $response->assertSee('Lista de roles');
        $response->assertSee('administrador');
        $response->assertSee('usuario');
    }
}
