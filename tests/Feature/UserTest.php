<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->get('/users');

        $response->assertStatus(200);
        $response->assertSee('id');
        $response->assertSee('nombre');
        $response->assertSee('alejandro');
        $response->assertSee('patricio');
        $response->assertSee('email');
        $response->assertSee('alejandro@gmail.com');
        $response->assertSee('role');
        $response->assertSee('administrador');
        $response->assertSee('usuario');
        $response->assertSee('Lista de usuarios');
    }
}
